﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class SpringZodiacSignOperationService : Generated.SpringZodiacSignService.SpringZodiacSignServiceBase
    {
        public override Task<ZodiacSignResponse> GetZodiacSign(SpringZodiacSignRequest request, ServerCallContext context)
        {
            string response = "";

            int month = (request.Date[0] - 48) * 10 + request.Date[1] - 48;
            int day = (request.Date[3] - 48) * 10 + request.Date[4] - 48;

            foreach (ZodiacSign sign in Server.signs)
            {
                if (sign.Begin.Month == month)
                {
                    if (sign.Begin.Day <= day)
                    {
                        response = sign.Name;
                        break;
                    }
                }
                else if (sign.End.Month == month)
                {
                    if (sign.End.Day >= day)
                    {
                        response = sign.Name;
                        break;
                    }
                }
            }

            return Task.FromResult(new ZodiacSignResponse() { ZodiacSign = response });
        }
    }
}
