// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: FallZodiacSignOperation.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Generated {

  /// <summary>Holder for reflection information generated from FallZodiacSignOperation.proto</summary>
  public static partial class FallZodiacSignOperationReflection {

    #region Descriptor
    /// <summary>File descriptor for FallZodiacSignOperation.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static FallZodiacSignOperationReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Ch1GYWxsWm9kaWFjU2lnbk9wZXJhdGlvbi5wcm90bxoYWm9kaWFjU2lnblJl",
            "c3BvbnNlLnByb3RvGhtGYWxsWm9kaWFjU2lnblJlcXVlc3QucHJvdG8yVQoV",
            "RmFsbFpvZGlhY1NpZ25TZXJ2aWNlEjwKDUdldFpvZGlhY1NpZ24SFi5GYWxs",
            "Wm9kaWFjU2lnblJlcXVlc3QaEy5ab2RpYWNTaWduUmVzcG9uc2VCDKoCCUdl",
            "bmVyYXRlZGIGcHJvdG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Generated.ZodiacSignResponseReflection.Descriptor, global::Generated.FallZodiacSignRequestReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, null));
    }
    #endregion

  }
}

#endregion Designer generated code
